﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Vidly.Models
{
    public class Movie
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "Please enter a movie name.")]
        [StringLength(255)]
        public string Name { get; set; }
        
        public Genre Genre { get; set; }

        [Required(ErrorMessage = "Genre is required.")]
        public byte GenreId { get; set; }

        [Required(ErrorMessage = "Date added is required.")]
        public DateTime DateAdded { get; set; }

        [Required(ErrorMessage = "Date released is required.")]
        public DateTime ReleaseDate { get; set; }

        [Required(ErrorMessage = "Number in stock is required.")]
        [Range(1,20, ErrorMessage = "Please enter a number between 1 and 20.")]
        public byte NumberInStock { get; set; }

        public byte NumberAvailable { get; set; }



    }
    
}