namespace Vidly.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class changedBirthDateFromStringToDateType : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Customers", "birthDate2", c => c.String());
            AlterColumn("dbo.Customers", "birthDate", c => c.DateTime());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Customers", "birthDate", c => c.String());
            DropColumn("dbo.Customers", "birthDate2");
        }
    }
}
