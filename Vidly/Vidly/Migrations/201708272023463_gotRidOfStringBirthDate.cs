namespace Vidly.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class gotRidOfStringBirthDate : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.Customers", "birthDate2");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Customers", "birthDate2", c => c.String());
        }
    }
}
