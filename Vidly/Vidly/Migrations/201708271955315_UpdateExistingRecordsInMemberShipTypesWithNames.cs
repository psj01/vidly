namespace Vidly.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdateExistingRecordsInMemberShipTypesWithNames : DbMigration
    {
        public override void Up()
        {
            Sql("Update MembershipTypes SET Name='Pay As You Go' Where Id = 1 ");
            Sql("Update MembershipTypes SET Name='Monthly' Where Id = 2 ");
            Sql("Update MembershipTypes SET Name='Quaterly' Where Id = 3  ");
            Sql("Update MembershipTypes SET Name='Yearly' Where Id = 4  ");
            
        }
        
        public override void Down()
        {
        }
    }
}
