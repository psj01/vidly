namespace Vidly.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SeedUsers : DbMigration
    {
        public override void Up()
        {
            Sql(@"
INSERT INTO [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'983c547d-a28a-4fa5-b3d0-6d25be38d9d8', N'guest@vidly.com', 0, N'AHyHKNoQod9CRTLVuSdl4TPiD8D/F3VLj0u7rPELJAeDkcWsWdc0HAfuzuKPDvmkxA==', N'2433eaf6-a178-4779-913f-dcf0d9f15287', NULL, 0, 0, NULL, 1, 0, N'guest@vidly.com')
INSERT INTO [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'aafbb1a2-26d4-4cf2-a8ba-0cd19ab0dd0f', N'admin@vidly.com', 0, N'ADzzIjL/7GPsDghTqARBzswwfXX5K1Di2XOl0sClLSzUYUwu3FJGil76K+aGwn89Yg==', N'e3f98fd7-f7bf-4589-8d49-f81e83a71bd7', NULL, 0, 0, NULL, 1, 0, N'admin@vidly.com')

INSERT INTO [dbo].[AspNetRoles] ([Id], [Name]) VALUES (N'e039d35b-9cd5-4c5a-a537-646863a4a390', N'CanManageMovie')

INSERT INTO [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'aafbb1a2-26d4-4cf2-a8ba-0cd19ab0dd0f', N'e039d35b-9cd5-4c5a-a537-646863a4a390')


");
        }
        
        public override void Down()
        {
        }
    }
}
