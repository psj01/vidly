// <auto-generated />
namespace Vidly.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class added_nbrAvailMoviesclass : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(added_nbrAvailMoviesclass));
        
        string IMigrationMetadata.Id
        {
            get { return "201709191855223_added_nbrAvailMoviesclass"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
