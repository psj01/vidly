﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Vidly.Models;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Data;
using System.Web.Configuration;
using Vidly.ViewModels;

namespace Vidly.Controllers
{
    public class CustomersController : Controller
    {

        private ApplicationDbContext _context;

        public CustomersController()
        {
            _context = new ApplicationDbContext();
        }
        

        protected override void Dispose(bool disposing)
        {
            _context.Dispose();
        }

        // GET: Customers
        public ActionResult Index()
        {
            //var customers = _context.Customers.Include(c => c.MembershipType).ToList();

            //return View(customers);
            return View();
        }


        public ActionResult Details(int? id)
        {
            var customer = _context.Customers.Include(c => c.MembershipType).SingleOrDefault(c => c.Id == id);

            if (customer == null)
            {
                return HttpNotFound();
            }

            return View(customer);

        }

        public ActionResult Edit(int? id)
        {
            var customer = _context.Customers.SingleOrDefault(c => c.Id == id);

            if(customer == null)
            {
                return HttpNotFound();
            }

            var viewModel = new CustomerFormViewModel
            {
                Customer = customer
                , MembershipTypes = _context.MembershipTypes.ToList()
            };

            return View("CustomerForm",viewModel);
        }

        public ActionResult New()
        {

            var membershipTypes = _context.MembershipTypes.ToList();

            var viewModel = new CustomerFormViewModel
            {
                Customer = new Customer(),
                MembershipTypes = membershipTypes
            };


            return View("CustomerForm", viewModel);
        }
        [HttpPost]
        [ValidateAntiForgeryToken] // <<<-- TO PREVENT CSRF ATTACKS! 
        public ActionResult Save(CustomerFormViewModel viewModel)
        {
            //validation
            if(!ModelState.IsValid)
            {
                var viewModel2 = new CustomerFormViewModel
                {
                    Customer = viewModel.Customer,
                    MembershipTypes = _context.MembershipTypes.ToList()
                };
                return View("CustomerForm", viewModel2);
            }

            if(viewModel.Customer.Id == 0)
            {
                // new customer so add as new
                _context.Customers.Add(viewModel.Customer);
            }
            else
            {
                //existing customer so update
                var customerInDB = _context.Customers.Single(c => c.Id == viewModel.Customer.Id);

                customerInDB.Name = viewModel.Customer.Name;
                customerInDB.birthDate = viewModel.Customer.birthDate;
                customerInDB.IsSubscribedToNewsLetter = viewModel.Customer.IsSubscribedToNewsLetter;
                customerInDB.MembershipTypeId = viewModel.Customer.MembershipTypeId;

            }


            _context.SaveChanges();

            return RedirectToAction("Index","Customers");
        }


        /* BELOW COMMENTED OUT STUFF IS FOR LEARNING PURPOSE ONLY!

        [HttpPost]
        public ActionResult test()
        {
            return RedirectToAction("Index", "Customers");
        }

        public ActionResult Details2(int? id)
        {


            var customer = _context.Customers.Include(c => c.MembershipType).SingleOrDefault(c => c.Id == id);

            if(customer==null)
            {
                return HttpNotFound();
            }

            //var movie1 = _context.Movies.SqlQuery("select * Movies where Customers.id = @id1",new SqlParameter("id1",1)).ToList();

            var movie1 = _context.Database.SqlQuery<result>("select Name from Customers where Customers.id = @id1", new SqlParameter("id1", 1)).ToList();

            ViewBag.movieName = movie1[0].Name;

            DataTable table = getData();

            ViewBag.table = table;

            return View(customer);

        }

        public struct result
        {
            //public int Id { get; set; }

            public string Name { get; set; }

            //public bool IsSubscribedToNewsLetter { get; set; }

            //public MembershipType MembershipType { get; set; }

            //public byte MembershipTypeId { get; set; }

            //public DateTime? birthDate { get; set; }
        }

        public DataTable getData()
        {


            SqlCommand com;
            SqlConnection conn;
            String strConn = WebConfigurationManager.ConnectionStrings["SqlAppConnection"].ConnectionString;
            conn = new SqlConnection(strConn);
            conn.Open();
            com = new SqlCommand("select * from vmsMaterialType where 1=1", conn);
            SqlDataAdapter da = new SqlDataAdapter(com);
            DataTable table = new DataTable();
            da.Fill(table);
            conn.Close();

            return table;
        }
        */

    }
}