﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Vidly.Dtos;
using Vidly.Models;

namespace Vidly.Controllers.Api
{
    public class NewRentalsController : ApiController
    {
        private ApplicationDbContext _context;

        public NewRentalsController()
        {
            _context = new ApplicationDbContext();
        }


        /* DO THIS WAY OR.... (THIS WAY IS BETTER FOR PUBLIC APIs)
        [HttpPost]
        public IHttpActionResult CreateNewRentals(NewRentalDto newRental)
        {
            if (newRental.MovieIds.Count == 0)
            {
                return BadRequest("No movie ids have been given.");
            }

            //get the customer
            var customer = _context.Customers.SingleOrDefault(c => c.Id == newRental.CustomerId);

            if(customer == null)
            {
                return BadRequest("Customer ID is not valid.");
            }

            //get the list of movies
            var movies = _context.Movies.Where(
                m => newRental.MovieIds.Contains(m.Id)).ToList();


            if(movies.Count != newRental.MovieIds.Count)
            {
                return BadRequest("One or more movie id's is invalid.");
            }

            // check to see if movies avail
            foreach(var movie in movies)
            {
                if (movie.NumberAvailable == 0)
                {
                    return BadRequest("Movie is not avail.");
                }

                movie.NumberAvailable--;


                var rental = new Rental
                {
                    Customer = customer,
                    Movie = movie,
                    DateRented = DateTime.Now
                };


                _context.Rentals.Add(rental);



            }

            _context.SaveChanges();

            return Ok();

        }
        */


            // SECOND WAY....

        [HttpPost]
        public IHttpActionResult CreateNewRentals(NewRentalDto newRental)
        {
            //get the customer
            var customer = _context.Customers.Single(c => c.Id == newRental.CustomerId);

            if (customer == null)
            {
                return BadRequest("Customer ID is not valid.");
            }

            //get the list of movies
            var movies = _context.Movies.Where(
                m => newRental.MovieIds.Contains(m.Id)).ToList();
            
            // check to see if movies avail
            foreach (var movie in movies)
            {
                if (movie.NumberAvailable == 0)
                {
                    return BadRequest("Movie is not avail.");
                }

                movie.NumberAvailable--;
                
                var rental = new Rental
                {
                    Customer = customer,
                    Movie = movie,
                    DateRented = DateTime.Now
                };


                _context.Rentals.Add(rental);



            }

            _context.SaveChanges();

            return Ok();

        }
    }
}
