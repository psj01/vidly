﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using Vidly.Models;

namespace Vidly.ViewModels
{
    public class MovieFormViewModel
    {
        public IEnumerable<Genre> Genres { get; set; }

        public int? Id { get; set; }

        [Required(ErrorMessage = "Please enter a movie name.")]
        [StringLength(255)]
        public string Name { get; set; }
        

        [Required(ErrorMessage = "Genre is required.")]
        public byte? GenreId { get; set; }

        [Required(ErrorMessage = "Date added is required.")]
        public DateTime? DateAdded { get; set; }

        [Required(ErrorMessage = "Date released is required.")]
        public DateTime? ReleaseDate { get; set; }

        [Required(ErrorMessage = "Number in stock is required.")]
        [Range(1, 20, ErrorMessage = "Please enter a number between 1 and 20.")]
        public byte? NumberInStock { get; set; }


        public string Title
        {
            get
            {
                if(Id != null && Id != 0)
                {
                    return "Edit Movie";
                }
                else
                {
                    return "New Movie";
                }
            }
        }

        public MovieFormViewModel()
        {
            Id = 0;
        }
        public MovieFormViewModel(Movie movie)
        {
            Id = movie.Id;
            Name = movie.Name;
            ReleaseDate = movie.ReleaseDate;
            NumberInStock = movie.NumberInStock;
            GenreId = movie.GenreId;
            DateAdded = movie.DateAdded;
        }

    }
}